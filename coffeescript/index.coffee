String::capitalize = () ->
  this.charAt(0).toUpperCase() + this.slice(1)

net        = require("net")
JSONStream = require('JSONStream')

serverHost = process.argv[2]
serverPort = process.argv[3]
botName = process.argv[4]
botKey = process.argv[5]

client = net.connect serverPort, serverHost, () ->
  send({ msgType: "join", data: { name: botName, key: botKey }})

send = (json) ->
  client.write JSON.stringify(json)
  client.write '\n'

class Car
  constructor: (@id) ->

  move: (angle, position, tick) ->

class Player extends Car
  constructor: (@id, @game) ->

  move: (angle, position, tick) ->
    distance = @game.track.distanceToFinishLine(position.lane.endLaneIndex, position.pieceIndex) - position.inPieceDistance

    if @previousDistance?
      [previousDistance, previousTick] = @previousDistance

      traveledDistance = previousDistance - distance
      elapsedTicks = tick-previousTick

      velocity = traveledDistance/elapsedTicks

      accelaration = (velocity - @previousVelocity)/elapsedTicks

      @previousVelocity = velocity

    @previousDistance = [distance,tick]

    console.log "[", tick, "] Distance: ", distance, " - Velocity: ", velocity, " m/ticks", " - Acceleration: ", accelaration, " m/ticks^2"

    send {msgType: "throttle", data: 0.5}

  onCrash: () ->
    console.log "Crashed!!"

class Track
  constructor:(@id, @name, @pieces, @lanes, @startingPoint) ->

  distanceToFinishLine: (lane, pieceIndex) ->

    currentLane = @lanes[lane]

    trackLength = 0

    for piece, i in @pieces when i >= pieceIndex
      # Straight piece
      if piece.length?
        trackLength += piece.length
      # Curve
      else if piece.radius? and piece.angle?
        lane = if piece.angle >= 0 then -currentLane.distanceFromCenter else currentLane.distanceFromCenter

        trackLength += ((piece.radius+lane)*Math.PI*Math.abs(piece.angle))/180

    return trackLength

class Game
  handle: (message) ->
    event = message.msgType
    event = event.capitalize()

    if event.indexOf("Game") == 0
      method = event[4..]

      @["on" + method](message.data)
    else if @["on" + event]?
      @["on" + event](message.data)

    @currentTick = message.gameTick if message.gameTick?

  onInit: (data) ->
    console.log "Game initialization"

    @track = data.race.track

    @track = new Track(@track.id, @track.name, @track.pieces, @track.lanes, @track.startingPoint)

    @cars = []

    for car in data.race.cars
      if car.id.name == @player.id.name
        car = @player = new Player(car.id, this)
      else
        car new Car(car.id)

      @cars.push car

    @session = data.race.raceSession

  onStart: (data) ->

  onEnd: (data) ->
    send {msgType: "ping", data: {}}

  onCarPositions: (data) ->
    for position in data
      for car in @cars when car.id.name == position.id.name
        car.move position.angle, position.piecePosition, @currentTick

  onCrash: (data) ->
    for car in @cars when car.name == data.name
      car.onCrash()

  onSpawn: (data) ->
    for car in @cars when car.name == data.name
      car.onSpawn()

  onYourCar: (data) ->
    @player = {id: data}

game = new Game()

jsonStream = client.pipe(JSONStream.parse())

jsonStream.on 'data', (data) ->
  game.handle data

jsonStream.on 'error', ->
  console.log "disconnected"
